# Defining a Macro Library

The purpose of this project is to show the neccessary steps of defining a custom macro library in liferay 7.x

## Creating the project

We will use blade-cli for creating our project. But first we need to know the version of the module we will be "overriding" (`com.liferay.portal.template.freemarker`)

### Finding the module version

1. Login with an admin account
2. Goto `Control Panel` -> `Configuration` -> `Gogo Shell`
3. Type into the command field `lb -s com.liferay.portal.template.freemarker` (lists bundles with symbolic name)
4. Note the bundle version

### Creating the project with blade

1. Remember the bundle version
2. Open a shell in your workspace dir
3. Execute the command

		blade create -t fragment -h com.liferay.portal.template.freemarker -H 4.0.18 YOUR_PROJ_NAME

## Adding the macros

1. Open your newly created project in an IDE of your choice
2. Create the file `src/main/resources/FTL_macros.ftl` and add your desired macros in it, eg.

		<#macro mymacro>
			<p>This is 'mymacro' by the custom macro library via fragment host.</p>
		</#macro>

3. Open a shell in the project dir and build it with

		gradle build

## Deployment

1. Deploy the jar file through one of the three deployment mechanisms
2. Goto `Control Panel` -> `Configuration` -> `System Settings` -> `Template Engines` -> scroll down to `Macro Library` and click (+) -> Add the text `FTL_macros.ftl as mylib` into the new textarea
3. Save
4. Restart the portal (otherwise you will get errors)
5. Use the macros in templates / themes like this

		<@mylib.mymacro></@mylib.mymacro>

## Sources

[Mechanism explanation for 7.x](https://liferay.dev/forums/-/message_boards/message/100627418)

[Mechanism explanation for 6.x](http://blog.learn-liferay.com/blog/2014/01/24/using-freemarker-macros-in-liferay-web-content-part-1/)

[Fragment Host](https://portal.liferay.dev/docs/7-1/tutorials/-/knowledge_base/t/jsp-overrides-using-osgi-fragments)

[Gogo Shell Commands](https://portal.liferay.dev/docs/7-1/reference/-/knowledge_base/r/using-the-felix-gogo-shell)

## Known issues / Improvements

* Check if we can use another type of project for this (since fragment host needs to override a core bundle and therefor the specific version of this bundle)
  * It would have to place its resources into the commonly available classpath
* Without deployment it is not possible to edit macros
